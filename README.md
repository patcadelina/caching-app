# README #

### What is this repository for? ###

* A REST Server demonstrating caching in Redis data store
* Version 0.0.1-SNAPSHOT

### How do I get set up? ###

This setup applies to an Ubuntu machine. If you are using a different machine, please check dependencies. Deployment process should be similar in most cases - run the Redis server and launch the REST server in Tomcat 7.

REST server uses Maven to handle dependencies, please make sure that you are online for dependency management.

#### Dependencies ####
* [Java 7](https://www.digitalocean.com/community/tutorials/how-to-install-java-on-ubuntu-with-apt-get)
* [Maven 3](http://maven.apache.org/download.cgi)
* [Redis 2.8](http://redis.io/topics/quickstart)
* [Tomcat 7](https://www.digitalocean.com/community/tutorials/how-to-install-apache-tomcat-7-on-ubuntu-14-04-via-apt-get)
* [Advanced Rest Client](https://chrome.google.com/webstore/detail/advanced-rest-client/hgmloofddffdnphfgcellkdfbfbjeloo)

#### Deployment Instructions ####
##### Redis data store #####
* cd <redis install directory>/src
* ./redis-server &
* ./redis-cli (optional -- to check persistent data)

##### REST server #####
* git clone <project url>
* cd <clone directory>
* mvn eclipse:eclipse (optional -- to view the source code in eclipse)
* mvn clean install -DskipTests=true
* mvn clean package -DskipTests=true
* sudo cp target/caching-app.war /var/lib/tomcat7/webapps/ (change tomcat webapp directory if installed in a different location)
* tail -f /var/lib/tomcat7/logs/catalina.out (check application startup)

##### How to run tests #####
* mvn clean test (API test requires the application running locally at port 8080)

### How to use the REST server? ###
The REST server was tested using Advanced REST client. A similar REST client can be used.

###### GET http://localhost:8080/caching-app/usernames?email=<username@domain.com> ######
* Recommend available username by email
* Response 200-OK, 500-Internal Server Error

###### GET http://localhost:8080/caching-app/usernames/{username} ######
* Query cached username info
* Response 200-OK, 404-Not Found, 500-Internal Server
