package com.freelancer.caching.repository;

import com.freelancer.caching.domain.CacheObject;

public interface CacheRepository<V extends CacheObject> {

	void add(V obj);

	String get(V obj);

	void delete(V obj);

}
