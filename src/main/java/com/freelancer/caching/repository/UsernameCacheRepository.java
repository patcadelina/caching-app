package com.freelancer.caching.repository;

import javax.annotation.Resource;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import com.freelancer.caching.domain.Username;

@Repository
public class UsernameCacheRepository implements CacheRepository<Username> {

	@Resource
	private RedisTemplate<String, String> redisTemplate;

	@Override
	public void add(Username username) {
		redisTemplate.opsForValue().set(username.getKey(), username.getCount());
	}

	@Override
	public String get(Username username) {
		return redisTemplate.opsForValue().get(username.getKey());
	}

	@Override
	public void delete(Username username) {
		redisTemplate.opsForValue().getOperations().delete(username.getKey());
	}

}
