package com.freelancer.caching.util;

public class CacheObjectUtil {

	public static final String KEY_ID_DELIMITER = ":";

	private CacheObjectUtil() {

	}

	public static String buildKey(String key, String identifier) {
		StringBuilder sb = new StringBuilder();
		sb.append(key);
		sb.append(KEY_ID_DELIMITER);
		sb.append(identifier);
		return sb.toString();
	}

}
