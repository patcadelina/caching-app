package com.freelancer.caching.util;

import com.freelancer.caching.domain.AvailableUsername;
import com.freelancer.caching.domain.Username;

public class RecommenderEngine {

	private RecommenderEngine() {

	}

	public static AvailableUsername recommendUsername(Username username) {
		Long count = Long.parseLong(username.getCount());
		if (count > 0) {
			StringBuilder sb = new StringBuilder();
			sb.append(username.getUsername());
			sb.append(count += 1);
			return AvailableUsername.newInstance(sb.toString());
		} else {
			return AvailableUsername.newInstance(username.getUsername());
		}
	}

}
