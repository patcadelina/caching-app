package com.freelancer.caching.util;

public class StringUtil {

	private StringUtil() {

	}

	public static String getEmailName(String email) {
		return email.split("@")[0];
	}

}
