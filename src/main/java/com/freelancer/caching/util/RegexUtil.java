package com.freelancer.caching.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUtil {

	public static final String USERNAME_REGEX = "[a-zA-Z]";
	public static final String COUNT_REGEX = "[0-9]";

	private RegexUtil() {

	}

	public static String match(String text, String regex) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(text);
		StringBuilder sb = new StringBuilder();
		while (matcher.find()) {
			sb.append(matcher.group());
		}
		return sb.toString();
	}

}
