package com.freelancer.caching.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.freelancer.caching.domain.Username;
import com.freelancer.caching.repository.UsernameCacheRepository;

@Component
public class CacheInitializer {

	private static final Logger log = LoggerFactory.getLogger(CacheInitializer.class);
	private static final String DATA_SOURCE = "usernames.txt";

	@Resource
	private UsernameCacheRepository repository;

	@PostConstruct
	public void initializeCache() throws IOException {
		BufferedReader inputStream = null;
		try {
			inputStream = new BufferedReader(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream(DATA_SOURCE)));
			String line;
			log.info("Retrieving data from source file...");
			while ((line = inputStream.readLine()) != null) {
				tryPersistUsername(buildUsernameCache(line));
			}
		} finally {
			if (inputStream != null) {
				inputStream.close();
			}
		}
	}

	private Username buildUsernameCache(String line) {
		String name = RegexUtil.match(line, RegexUtil.USERNAME_REGEX);
		String count = RegexUtil.match(line, RegexUtil.COUNT_REGEX);
		return Username.newInstance(name, count.length() == 0 ? "0" : count);
	}

	private void tryPersistUsername(Username username) {
		String cacheCount = repository.get(username);
		if (cacheCount == null) {
			repository.add(username);
		} else {
			if (Long.valueOf(username.getCount()) > Long.valueOf(cacheCount)) {
				repository.add(username);
			}
		}
	}

}
