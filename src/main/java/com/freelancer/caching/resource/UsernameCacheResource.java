package com.freelancer.caching.resource;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freelancer.caching.domain.AvailableUsername;
import com.freelancer.caching.domain.Username;
import com.freelancer.caching.dto.UsernameDto;
import com.freelancer.caching.exception.NotFoundException;
import com.freelancer.caching.service.UsernameCacheService;
import com.freelancer.caching.util.RecommenderEngine;
import com.freelancer.caching.util.StringUtil;

@Controller
@RequestMapping("/usernames")
public class UsernameCacheResource {

	private static final Logger log = LoggerFactory.getLogger(UsernameCacheResource.class);

	@Resource
	private UsernameCacheService service;

	@RequestMapping(method = RequestMethod.GET, value = "/{username}")
	@ResponseBody
	public ResponseEntity<UsernameDto> findByUsername(@PathVariable String username) {
		try {
			Username queryResult = service.getUsername(Username.newInstance(username, null));
			return new ResponseEntity<UsernameDto>(UsernameDto.newInstance(queryResult), HttpStatus.OK);
		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			log.debug(e.getMessage());
			return new ResponseEntity<UsernameDto>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AvailableUsername> findAvailableByEmail(@RequestParam(value="email") String email) {
		try {
			Username queryResult = service.getUsername(Username.newInstance(StringUtil.getEmailName(email), null));
			return new ResponseEntity<AvailableUsername>(RecommenderEngine.recommendUsername(queryResult),
					HttpStatus.OK);
		} catch (NotFoundException e) {
			Username username = Username.newInstance(StringUtil.getEmailName(email), "0");
			return new ResponseEntity<AvailableUsername>(RecommenderEngine.recommendUsername(username),
					HttpStatus.OK);
		} catch (Exception e) {
			log.debug(e.getMessage());
			return new ResponseEntity<AvailableUsername>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
