package com.freelancer.caching.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.freelancer.caching.domain.Username;
import com.freelancer.caching.exception.NotFoundException;
import com.freelancer.caching.repository.UsernameCacheRepository;

@Service
public class UsernameCacheService {

	@Resource
	private UsernameCacheRepository repository;

	public Username getUsername(Username username) throws NotFoundException {
		String count = repository.get(username);
		if (isUsernameFound(count)) {
			return Username.newInstance(username.getUsername(), count);
		}
		throw new NotFoundException();
	}

	private boolean isUsernameFound(String count) {
		return !(count == null || count.equals(""));
	}

}
