package com.freelancer.caching.dto;

import java.io.Serializable;

import com.freelancer.caching.domain.Username;

public class UsernameDto implements Serializable {

	private static final long serialVersionUID = 4459305446618077902L;

	private String username;

	private Long count;

	public UsernameDto() {

	}

	private UsernameDto(String username, Long count) {
		this.username = username;
		this.count = count;
	}

	public static UsernameDto newInstance(Username username) {
		return new UsernameDto(username.getUsername(), Long.parseLong(username.getCount()));
	}

	public String getUsername() {
		return username;
	}

	public Long getCount() {
		return count;
	}

}
