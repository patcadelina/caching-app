package com.freelancer.caching.domain;

import com.freelancer.caching.util.CacheObjectUtil;

public class Username implements CacheObject {

	private static final long serialVersionUID = 5930292645589027752L;
	public static final String KEY = "username";
	private String username;
	private String count;

	public Username() {

	}

	private Username(String username, String count) {
		this.username = username;
		this.count = count;
	}

	public static Username newInstance(String username, String count) {
		return new Username(username, count);
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	@Override
	public String getKey() {
		return CacheObjectUtil.buildKey(KEY, username);
	}

	@Override
	public String toString() {
		return "Username [username=" + username + ", count=" + count + "]";
	}

}
