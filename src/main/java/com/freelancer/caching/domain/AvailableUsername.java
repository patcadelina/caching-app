package com.freelancer.caching.domain;

import java.io.Serializable;

public class AvailableUsername implements Serializable {

	private static final long serialVersionUID = -3869080839112927656L;

	private String username;

	public AvailableUsername() {

	}

	private AvailableUsername(String username) {
		this.username = username;
	}

	public static AvailableUsername newInstance(String username) {
		return new AvailableUsername(username);
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
