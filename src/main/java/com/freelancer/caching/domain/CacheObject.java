package com.freelancer.caching.domain;

import java.io.Serializable;

public interface CacheObject extends Serializable {

	String getKey();

}
