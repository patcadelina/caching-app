package com.freelancer.caching.resource;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import com.freelancer.caching.domain.AvailableUsername;
import com.freelancer.caching.domain.Username;

public class UsernameCacheResourceTest {

	private RestTemplate template = new RestTemplate();

	@Test
	public void shouldRetrieveExistingUser() {
		String url = "http://localhost:8080/caching-app/usernames/{username}";
		Username username = template.getForObject(url, Username.class, "john");
		Assert.assertNotNull(username);
		Assert.assertTrue(username.getUsername().equals("john"));
	}

	@Test
	public void shouldRecommendNextCount() {
		String url = "http://localhost:8080/caching-app/usernames?email=pat@yahoo.com";
		AvailableUsername username = template.getForObject(url, AvailableUsername.class);
		Assert.assertTrue(username.getUsername().equals("pat107"));
	}

}
