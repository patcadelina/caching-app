package com.freelancer.caching.repository;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.freelancer.caching.domain.Username;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/application-config-test.xml" })
public class UsernameCacheRepositoryTest {

	private static final String ANY_KEY = Mockito.anyString();
	private static final String ANY_COUNT = "1988";

	@Resource
	private UsernameCacheRepository repository;

	@Test
	public void shouldDeleteFromCache() {
		Username username = Username.newInstance(ANY_KEY, ANY_COUNT);
		repository.add(username);
		Assert.assertNotNull(repository.get(username));

		repository.delete(username);
		Assert.assertNull(repository.get(username));
	}

	@Test
	public void shouldAddToCache() {
		Username username = Username.newInstance(ANY_KEY, ANY_COUNT);
		Assert.assertNull(repository.get(username));

		repository.add(username);
		Assert.assertNotNull(repository.get(username));

		cleanUp(username);
	}

	private void cleanUp(Username username) {
		repository.delete(username);
	}

	@Test
	public void shouldGetCacheCount() {
		Username username = Username.newInstance(ANY_KEY, ANY_COUNT);
		repository.add(username);

		String count = repository.get(username);
		Assert.assertEquals(ANY_COUNT, count);

		cleanUp(username);
	}

	@Test
	public void shouldReturnNullOnNonExistingKey() {
		Username username = Username.newInstance(ANY_KEY, ANY_COUNT);
		Assert.assertNull(repository.get(username));
	}

}
