package com.freelancer.caching.util;

import org.junit.Assert;
import org.junit.Test;

public class StringUtilTest {

	@Test
	public void shouldGetEmailName() {
		String expected = "charm";
		String actual = StringUtil.getEmailName("charm@freelancer.com");
		Assert.assertTrue(expected.equals(actual));
	}

}
