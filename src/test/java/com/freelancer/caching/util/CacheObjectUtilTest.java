package com.freelancer.caching.util;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class CacheObjectUtilTest {

	private static final String ANY_KEY = Mockito.anyString();
	private static final String ANY_IDENTIFIER = Mockito.anyString();

	@Test
	public void shouldBuildKey() {
		String expected = ANY_KEY + CacheObjectUtil.KEY_ID_DELIMITER + ANY_IDENTIFIER;
		String actual = CacheObjectUtil.buildKey(ANY_KEY, ANY_IDENTIFIER);
		Assert.assertEquals(expected, actual);
	}

}
