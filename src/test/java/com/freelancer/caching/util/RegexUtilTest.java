package com.freelancer.caching.util;

import org.junit.Assert;
import org.junit.Test;

public class RegexUtilTest {

	@Test
	public void shouldMatchUsername() {
		String text = "regex101";
		String expected = "regex";
		String actual = RegexUtil.match(text, RegexUtil.USERNAME_REGEX);
		Assert.assertTrue(expected.equals(actual));
	}

	@Test
	public void shouldMatchCount() {
		String text = "regex101";
		String expected = "101";
		String actual = RegexUtil.match(text, RegexUtil.COUNT_REGEX);
		Assert.assertTrue(expected.equals(actual));
	}

	@Test
	public void shouldReturnEmptyString() {
		String text = "regex";
		String expected = "";
		String actual = RegexUtil.match(text, RegexUtil.COUNT_REGEX);
		Assert.assertTrue(expected.equals(actual));
	}

}
