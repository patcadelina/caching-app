package com.freelancer.caching.util;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.freelancer.caching.domain.AvailableUsername;
import com.freelancer.caching.domain.Username;

public class RecommenderEngineTest {

	private static final String ANY_NAME = "pat";
	private static final String ANY_NON_ZERO_COUNT = "72488";
	private static final String ZERO_COUNT = "0";

	@Test
	public void shouldIncrementCount() {
		Username username = Mockito.mock(Username.class);
		Mockito.when(username.getUsername()).thenReturn(ANY_NAME);
		Mockito.when(username.getCount()).thenReturn(ANY_NON_ZERO_COUNT);
		StringBuilder sb = new StringBuilder();
		sb.append(ANY_NAME);
		sb.append(Long.parseLong(ANY_NON_ZERO_COUNT) + 1);
		AvailableUsername expected = AvailableUsername.newInstance(sb.toString());
		AvailableUsername actual = RecommenderEngine.recommendUsername(username);
		Assert.assertTrue(expected.getUsername().equals(actual.getUsername()));
	}

	@Test
	public void shouldReturnNameAsIs() {
		Username username = Mockito.mock(Username.class);
		Mockito.when(username.getUsername()).thenReturn(ANY_NAME);
		Mockito.when(username.getCount()).thenReturn(ZERO_COUNT);
		AvailableUsername expected = AvailableUsername.newInstance(ANY_NAME);
		AvailableUsername actual = RecommenderEngine.recommendUsername(username);
		Assert.assertTrue(expected.getUsername().equals(actual.getUsername()));
	}

}
